//
//  MainTabBarViewController.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    var statsViewController = StatsViewController()
    var settingsViewCotnroller = SettingsViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        let viewControllers: [UIViewController] = [
            statsViewController.navigationControllerSurroundingViewController(),
            settingsViewCotnroller.navigationControllerSurroundingViewController()
        ]
        
        setViewControllers(viewControllers, animated: false)
    }
}
