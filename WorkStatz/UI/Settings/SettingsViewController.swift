//
//  SettingsViewController.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import Eureka

class SettingsViewController: FormViewController {
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        title = "Settings"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++ Section("Set work location")
            <<< TextRow("workAddress"){ row in
                row.placeholder = "Work address"
                row.baseValue = UserSettings.shared.getCurrentGeoLocation()?.formatted_address
            }
            <<< ButtonRow() { row in
                row.title = "Set"
                row.onCellSelection({ [weak self] (buttonCell, buttonRow) in
                    guard let `self` = self else { return }
                    let textRow = self.form.rowBy(tag: "workAddress")!
                    let addressToSearch = textRow.baseValue as! String
                    
                    textRow.baseCell.isUserInteractionEnabled = false
                    
                    UserSettings.shared.setWorkAddress(searchPhrase: addressToSearch) { (localizedAddress, error) in
                        textRow.baseCell.isUserInteractionEnabled = true
                        
                        if error != nil { return }
                        
                        textRow.baseValue = localizedAddress
                        textRow.reload()
                    }
                })
        }
    }
}

