//
//  StatsViewController.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

class StatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let statsTableView = UITableView()
    var stats: UpdatingCollection<UserWorkTimeLog>? = nil
    var invalidateUpdateBlock: (()->Void)? = nil
    

    init() {
        super.init(nibName: nil, bundle: nil)
        title = "Stats"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(statsTableView)
        statsTableView.frame = view.bounds
        statsTableView.delegate = self
        statsTableView.dataSource = self
        statsTableView.register(StatsTableViewCell.self, forCellReuseIdentifier: StatsTableViewCell.cellIdentifier())
        statsTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        statsTableView.allowsSelection = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let (results, invalidateUpdateBlock) = UserTimeLogs.shared.getUserTimeLogs {
            self.statsTableView.reloadData()
        }
        
        self.stats = results
        self.invalidateUpdateBlock = invalidateUpdateBlock
        self.statsTableView.reloadData()
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let stats = stats else {
            return 0
        }
        return stats.count()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return StatsTableViewCell.heightForCell(width: statsTableView.frame.width)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StatsTableViewCell.cellIdentifier(), for: indexPath) as! StatsTableViewCell
        guard let stats = stats else {
            return cell
        }
        
        let statsForIndexPath = stats[indexPath.row]
        
        cell.startTime = statsForIndexPath.createdAt
        cell.endTime = statsForIndexPath.createdAt
        
        return cell
    }

}
