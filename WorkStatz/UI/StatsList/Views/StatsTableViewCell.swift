//
//  StatsTableViewCell.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

class StatsTableViewCell: UITableViewCell {
    
    private let dateFormatter = DateFormatter()
    
    private let endTimeImageView: UIImageView = {
        guard var image = UIImage(named: "ic_assignment_return") else { return UIImageView() }
        image = image.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        let imageView = UIImageView(image: image)
        imageView.tintColor = UIColor.red
        return imageView
    }()
    
    private let startTimeImageView: UIImageView = {
        guard let image = UIImage(named: "ic_assignment_return") else { return UIImageView() }
        let mirroredImage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: UIImageOrientation.upMirrored).withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        let imageView = UIImageView(image: mirroredImage)
        imageView.tintColor = UIColor.green
        return imageView
    }()
    
    private let startTimeLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private let endTimeLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    public var startTime: Date? = nil {
        didSet {
            if let startTime = startTime {
                self.startTimeLabel.text = dateFormatter.string(from: startTime)
            } else {
                self.startTimeLabel.text = nil
            }
        }
    }
    
    public var endTime: Date? = nil {
        didSet {
            if let endTime = endTime {
                self.endTimeLabel.text = dateFormatter.string(from: endTime)
            } else {
                self.endTimeLabel.text = nil
            }
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(startTimeImageView)
        addSubview(endTimeImageView)
        addSubview(startTimeLabel)
        addSubview(endTimeLabel)
        
        
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = NSLocale.current
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        startTimeImageView.frame = CGRect.init(x: 15, y: 10, width: 30, height: 30)
        startTimeLabel.frame = CGRect.init(x: startTimeImageView.right + 15, y: 10, width: bounds.size.width - 60, height: 30)
        endTimeImageView.frame = CGRect.init(x: 15, y: startTimeImageView.bottom + 10, width: 30, height: 30)
        endTimeLabel.frame = CGRect.init(x: startTimeImageView.right + 15, y: startTimeImageView.bottom + 10, width: bounds.size.width - 60, height: 30)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        startTime = nil
        endTime = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static func cellIdentifier() -> String {
        return "StatsTableViewCellIdentifier"
    }
    
    static func heightForCell(width: CGFloat) -> CGFloat {
        return 90
    }
}
