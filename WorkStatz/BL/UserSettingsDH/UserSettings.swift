//
//  UserSettings.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

protocol UserSettingsDelegate {
    func geoLocationChanged()
}

class UserSettings: NSObject {
    static let shared = UserSettings()
    private let geolocationUserDefaultsKey = "UserSettings.Geolocation"
    var delegate: UserSettingsDelegate? = nil

    func setWorkAddress(searchPhrase: String, completionBlock: @escaping (_ localizedWorkAddress: String?, _ error: Error?)->Void) {        
        GoogleGeocodeService.getLocation(searchTerm: searchPhrase) { (geolocation, error) in
            if error != nil {
                completionBlock(nil, error)
                return
            }
            
            self.setCurrentGeoLocation(geolocation)
            completionBlock(geolocation?.formatted_address, nil)
        }
    }
    
    func getCurrentGeoLocation() -> Geolocation? {        
        let geocodeDataOptional = UserDefaults.standard.object(forKey: geolocationUserDefaultsKey) as? Data
        guard let geocodeData = geocodeDataOptional else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: geocodeData) as? Geolocation
    }
    
    private func setCurrentGeoLocation(_ geolocation: Geolocation?) {
        if let geolocation = geolocation {
            let geolocationData = NSKeyedArchiver.archivedData(withRootObject: geolocation)
            UserDefaults.standard.set(geolocationData, forKey: geolocationUserDefaultsKey)
        } else {
            UserDefaults.standard.removeObject(forKey: geolocationUserDefaultsKey)
        }
        UserDefaults.standard.synchronize()
        
        delegate?.geoLocationChanged()
    }
    
    
}
