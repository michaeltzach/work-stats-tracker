//
//  UserTimeLogs.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import RealmSwift

protocol UserTimeLogsDataHandlerProtocol {
    func getUserTimeLogs(resultsUpdatedBlock: @escaping () -> Void) ->
        (results: UpdatingCollection<UserWorkTimeLog>, invalidateUpdateBlock: () -> ())
    
    func create(userTimeLog: UserWorkTimeLog) -> Bool
}

class UserTimeLogs: UserTimeLogsDataHandlerProtocol {
    let realm = try! Realm()
    var notificationTokens = [NotificationToken]()
    
    static let shared = UserTimeLogs()
    
    func getUserTimeLogs(resultsUpdatedBlock: @escaping () -> Void) ->
                        (results: UpdatingCollection<UserWorkTimeLog>, invalidateUpdateBlock: () -> ())  {
        let results: Results<UserWorkTimeLog> = realm.objects(UserWorkTimeLog.self).sorted(byKeyPath: "createdAt", ascending: false)
        
        let notificationToken: NotificationToken = results.addNotificationBlock { (changes: RealmCollectionChange) in
            resultsUpdatedBlock()
        }
        notificationTokens.append(notificationToken)
        
        func invalidateUpdateBlock() -> () {
            if let indexToRemove = self.notificationTokens.index(where: { return $0 == notificationToken }) {
                self.notificationTokens.remove(at: indexToRemove)
            }
        }
        return (UpdatingCollection.init(results: results), invalidateUpdateBlock)
    }
    
    func create(userTimeLog: UserWorkTimeLog) -> Bool {
        do {
            try realm.write {
                realm.add(userTimeLog)
            }
            return true
        } catch {
            return false
        }
    }
}
