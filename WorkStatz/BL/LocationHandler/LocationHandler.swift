//
//  LocationHandler.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import CoreLocation

class LocationHandler: NSObject, UserSettingsDelegate, CLLocationManagerDelegate {

    static let shared = LocationHandler()
    private let locationManager = CLLocationManager.init()
    
    private var dateUserEnteredRegion: Date? = nil
    
    func startService() {
        UserSettings.shared.delegate = self
        locationManager.delegate = self
        
        registerRegionIfNeeded()
    }
    
    func registerRegionIfNeeded() {
        guard let geolocation = UserSettings.shared.getCurrentGeoLocation() else { return }
        if CLLocationManager.locationServicesEnabled() == false {
            return
        }
        
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted { return }
        
        var radius: CLLocationDistance = 50
        if radius > locationManager.maximumRegionMonitoringDistance {
            radius = locationManager.maximumRegionMonitoringDistance
        }
        
        let center = CLLocationCoordinate2DMake(geolocation.lat, geolocation.lng)
        
        let region = CLCircularRegion.init(center: center, radius: radius, identifier: "LocationHandler.regionIdentifier")
        
        locationManager.startMonitoring(for: region)
    }
    
    // MARK: UserSettingsDelegate
    
    func geoLocationChanged() {
        for regionToDelete in locationManager.monitoredRegions {
            locationManager.stopMonitoring(for: regionToDelete)
        }
        
        registerRegionIfNeeded()
    }
    
    // MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        dateUserEnteredRegion = Date.init()
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        guard let dateUserEnteredRegion = dateUserEnteredRegion else { return }
        
        let userWorkTimeLog = UserWorkTimeLog.init()
        userWorkTimeLog.startTime = dateUserEnteredRegion
        userWorkTimeLog.endTime = Date()
        userWorkTimeLog.createdAt = Date()
        _ = UserTimeLogs.shared.create(userTimeLog: userWorkTimeLog)
    }
    
}
