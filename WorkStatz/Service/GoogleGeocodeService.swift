//
//  GoogleGeocodeService.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import Alamofire

class GoogleGeocodeService: NSObject {
    static let apiKey = "AIzaSyAGsiUHQ4wumhiW9eE0HPZ__sjkW0RvC1E"
    static let baseURL = URL.init(string:"https://maps.googleapis.com/maps/api/geocode/json")!
    
    static func getLocation(searchTerm: String, completionBlock: @escaping (Geolocation?, Error?)->Void) {
        let parameters: [String: String] = [
            "key": apiKey,
            "address": searchTerm.replacingOccurrences(of: " ", with: "+")
        ]
        
        Alamofire.request(baseURL, method: .get, parameters: parameters).responseJSON { response in
            guard let dict = response.result.value as? [String: Any] else {
                completionBlock(nil, response.error)
                return
            }
            
            let geoLocation = Geolocation(withData: dict)
            completionBlock(geoLocation, nil)
        }
    }
}
