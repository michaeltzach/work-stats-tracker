//
//  Geolocation.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

class Geolocation: NSObject, NSCoding {
    var formatted_address: String = ""
    var lat: Double!
    var lng: Double!
    
    init?(withData data: [String:Any]) {
        super.init()
        
        guard let arrayOfResults = data["results"] as? [Any] else { return }
        if (arrayOfResults.count < 1) { return }
        
        guard let firstResult = arrayOfResults[0] as? [String: Any] else { return }
        guard let formatted_address = firstResult["formatted_address"] as? String else { return nil }
        self.formatted_address = formatted_address
        
        guard let geometry = firstResult["geometry"] as? [String: Any] else { return }
        guard let location = geometry["location"] as? [String: Any] else { return }
        guard let lat = location["lat"] as? Double else { return nil }
        self.lat = lat
        guard let lng = location["lng"] as? Double else { return nil }
        self.lng = lng
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init()
        
        guard let formatted_address = aDecoder.decodeObject(forKey: "formatted_address") as? String else { return nil }
        self.formatted_address = formatted_address
        guard let latString = aDecoder.decodeObject(forKey: "lat") as? String else { return nil }
        self.lat = Double(latString)
        guard let lngString = aDecoder.decodeObject(forKey: "lng") as? String else { return nil }
        self.lng = Double(lngString)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(formatted_address, forKey: "formatted_address")
        aCoder.encode(lat.description, forKey: "lat")
        aCoder.encode(lng.description, forKey: "lng")
    }
}
