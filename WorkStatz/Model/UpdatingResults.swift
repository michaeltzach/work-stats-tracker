//
//  UpdatingResults.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import RealmSwift

class UpdatingCollection<T: Object>: NSObject {
    private var results: Results<T>!
    
    init(results: Results<T>) {
        super.init()
        self.results = results
    }
    
    func count() -> Int {
        return results.count
    }
    
    subscript(position: Int) -> T {
        return results[position]
    }
}
