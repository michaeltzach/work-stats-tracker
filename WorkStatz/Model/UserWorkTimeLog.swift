//
//  UserWorkTimeLog.swift
//  WorkStatz
//
//  Created by Michael tzach on 30/11/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import RealmSwift

class UserWorkTimeLog: Object {
    @objc dynamic var startTime: Date?
    @objc dynamic var endTime: Date?
    @objc dynamic var createdAt: Date?
    
    func totalTime() -> TimeInterval {
        guard let startTime = startTime else { return 0 }
        guard let endTime = endTime else { return 0 }
        
        return endTime.timeIntervalSince(startTime)
    }
}
